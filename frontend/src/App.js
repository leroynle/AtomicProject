import React, { Component } from "react";
import Layout1 from "../src/pages/Layout1/Layout1";
import Layout2 from "../src/pages/Layout2/Layout2";
import Layout3 from "../src/pages/Layout3/Layout3";
import Layout4 from "../src/pages/Layout4/Layout4";
import Layout5 from "../src/pages/Layout5/Layout5";
import Layout6 from "../src/pages/Layout6/Layout6";
import Login from "../src/component/Login";
import SignUp from "../src/component/SignUp";
import ForgotPassword from "../src/component/ForgotPassword";
import {
  Route,
  Routes
} from "react-router-dom";
import "./assets/css/materialdesignicons.min.css";

import "./assets/scss/themes.scss";

class App extends Component {
  render() {
    return (
          <Routes>
            <Route path="/" element={<Layout1/> }></Route>
            <Route path="/Layout2" element={<Layout2/> }></Route>
            <Route path="/Layout3" element={<Layout3/> }></Route>
            <Route path="/Layout4" element={<Layout4/> }></Route>
            <Route path="/Layout5" element={<Layout5/> }></Route>
            <Route path="/Layout6" element={<Layout6/> }></Route>
            <Route path="/Login" element={<Login/> }></Route>
            <Route path="/SignUp" element={<SignUp/> }></Route>
            <Route path="/ForgotPassword" element={<ForgotPassword/> }></Route>
          </Routes>
    );
  }
}

export default App;
