import React from "react";
import { Col, Container, Form, FormGroup, Row, Button, Input, Label, FormFeedback } from "reactstrap";
import { Link } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import Feature4 from '../assets/images/features/img-4.png';
import LogoDark from '../assets/images/logo-dark.png';
import axios from 'axios';

const schema = Yup.object({
  email: Yup
    .string()
    .email()
    .required('Please Enter your Email'),
  password: Yup
    .string()
    .required("No password provided.")
    .min(6, "Password is too short - should be 8 chars minimum.")
    .matches(/(?=.*[0-9])/, "Password must contain a number."),
});

const SignUp = () => {
  return (
  <Formik
    validationSchema={schema}
    initialValues={{
      email : "",
      password: "",
    }}
    onSubmit={async (values, actions) =>{
      actions.setSubmitting(true)
        try{
          const res = await axios.post(`http://localhost:8080/api/v1/auths/register`,
            JSON.stringify(values)
          )
          const response = res.data
        console.log("successfully created account",response)
      } catch(e){
        console.error(e); 
      } finally{
        actions.setSubmitting(false)
      }
    }}
    validator={() => ({})}
  >
  {({
    handleSubmit,
    handleChange,
    values,
    errors,
  }) => (
    <>
        <div className="account-home-btn d-none d-sm-block">
        <Link to="/" className="text-primary"><i className="mdi mdi-home h1"></i></Link>
        </div>

        <section className="bg-account-pages vh-100">
          <div className="display-table">
            <div className="display-table-cell">
              <Container>
                <Row className="no-gutters align-items-center">
                  <Col lg={12}>
                    <div className="login-box">
                      <Row className="align-items-center no-gutters">
                        <Col lg={6}>
                          <div className="bg-light">
                            <div className="row justify-content-center">
                              <div className="col-lg-10">
                                <div
                                  className="home-img login-img text-center d-none d-lg-inline-block">
                                  <div className="animation-2"></div>
                                  <div className="animation-3"></div>
                                  <img src={Feature4} className="img-fluid" alt="" />
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col lg={6}>
                          <Row className="justify-content-center">
                            <Col lg={11}>
                              <div className="p-4">
                                <div className="text-center mt-3">
                                  <Link to="#"><img src={LogoDark} alt=""
                                    height="22" /></Link>
                                  <p className="text-muted mt-3">Sign up for a new Account</p>
                                </div>
                                <div className="p-3 custom-form">
                                  <Form onSubmit={handleSubmit}>
                                    <FormGroup>
                                      <Label for="email">Email</Label>
                                      <Input 
                                      type="email"
                                      className="form-control" 
                                      id="email" 
                                      placeholder="Enter Email"
                                      invalid={!!errors.email}
                                      name="email"
                                      onChange={handleChange}
                                      value={values.email}
                                      />
                                       <FormFeedback className="FeedBack" type="invalid">
                                      {errors.email}
                                    </FormFeedback>
                                    </FormGroup>
                                   
                                    <FormGroup>
                                      <Label for="userpassword">Password</Label>
                                      <Input 
                                      type="password" 
                                      className="form-control" 
                                      name="password"
                                      id="userpassword" 
                                      placeholder="Enter Password" 
                                      invalid={!!errors.password}
                                      onChange={handleChange}
                                      value={values.password}
                                      />
                                      <FormFeedback className="FeedBack" type="invalid">
                                        {errors.password}
                                      </FormFeedback>
                                    </FormGroup>
                                 
                                    <div className="mt-3">
                                      <Button type="submit" color="primary" className="btn btn-primary btn-block">Sign Up</Button>{" "}
                                    </div>
                                    <div className="mt-4 pt-1 mb-0 text-center">
                                      <p className="mb-0">Have an account already?
                                      <Link to="/Login" className="text-success" > Sign in</Link></p>
                                    </div>
                                  </Form>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>
        </>
)}
</Formik>
)};
export default SignUp;