import React from "react";
import { Col, Container, Form, FormGroup, Row, Button, Input, Label,FormFeedback } from "reactstrap";
import { Link } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import Feature4 from '../assets/images/features/img-4.png';
import LogoDark from '../assets/images/logo-dark.png';
import axios from 'axios'


const schema = Yup.object({
  email: Yup
    .string()
    .email()
    .required('Please Enter your Email'),
  password: Yup
    .string()
    .required("No password provided."),
});
const Login = () => {

    return (
      <Formik
        validationSchema={schema}
        initialValues={{
          email : "",
          password: "",
        }}
        onSubmit= {async (values, actions) =>{
          actions.setSubmitting(true)
          try{
          const res = await axios.post(`http://localhost:8080/api/v1/auths/login`,
            JSON.stringify(values)
          )
            const token = res.data.jwt
            console.log("successfully logged in", token)
          } catch(e){
            console.error(e);
          } finally{
            actions.setSubmitting(false)
          }
          
        }}
        validator={() => ({})}
      >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
      }) => (
        <>
        <div className="account-home-btn d-none d-sm-block">
          <Link to="/" className="text-primary"><i className="mdi mdi-home h1"></i></Link>
        </div>
        <section className="bg-account-pages vh-100">
          <div className="display-table">
            <div className="display-table-cell">
              <Container>
                <Row>
                  <Col lg={12}>
                    <div className="login-box">
                      <Row className="align-items-center no-gutters">
                        <Col lg={6}>
                          <div className="bg-light">
                            <div className="row justify-content-center">
                              <div className="col-lg-10">
                                <div
                                  className="home-img login-img text-center d-none d-lg-inline-block">
                                  <div className="animation-2"></div>
                                  <div className="animation-3"></div>
                                  <img src={Feature4} className="img-fluid" alt="" />
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col lg={6}>
                          <Row className="justify-content-center">
                            <Col lg={11}>
                              <div className="p-4">
                                <div className="text-center mt-3">
                                <Link to="#"><img src={LogoDark} alt=""
                                    height="22" /></Link>
                                  <p className="text-muted mt-3">Sign in to continue to Atomic.</p>
                                </div>
                                <div className="p-3 custom-form">
                                  <Form onSubmit={handleSubmit}>
                                    <FormGroup>
                                      <Label for="email">Email</Label>
                                      <Input type="email" 
                                            className="form-control" 
                                            id="email"
                                            placeholder="Enter Email" 
                                            invalid={!!errors.email}
                                            onChange={handleChange}
                                            value={values.email}
                                            />
                                         <FormFeedback className="FeedBack" type="invalid">
                                          {errors.email}
                                        </FormFeedback>
                                    </FormGroup>
                                    <FormGroup>
                                      <Label for="userpassword">Password</Label>
                                      <Input type="password" 
                                      className="form-control"
                                        id="userpassword" 
                                        name="password"
                                        placeholder="Enter password"
                                        invalid={!!errors.password}
                                      onChange={handleChange}
                                      value={values.password} />
                                      <FormFeedback className="FeedBack" type="invalid">
                                        {errors.password}
                                      </FormFeedback>
                                    </FormGroup>
                                    {/* <div className="custom-control custom-checkbox">
                                      <Input type="checkbox" className="custom-control-input"
                                        id="customControlInline" />
                                      <Label className="custom-control-label"
                                        for="customControlInline">Remember me</Label>
                                    </div> */}
                                    <div className="mt-3">
                                      <Button type="submit" color="primary" className="btn btn-primary btn-block">Log In</Button>{" "}
                                    </div>
                                    <div className="mt-4 pt-1 mb-0 text-center">
                                      <Link to="/ForgotPassword" className="text-dark"><i
                                        className="mdi mdi-lock"></i> Forgot your
                                                                    password?</Link>
                                    </div>
                                    <div className="mt-4 pt-1 mb-0 text-center">
                                      <Link to="/Signup" className="text-success"> Sign up for free</Link>
                                    </div>
                                  </Form>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </section>
        </>
    )}

  </Formik>
)};

export default Login;
