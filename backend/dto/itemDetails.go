package dto

import (
	"time"

	"gorm.io/gorm"
)

type Item struct {
	ItemID           uint `gorm:"primaryKey"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        gorm.DeletedAt `gorm:"index"`
	URL              string         `json:"url"`
	ItemName         string         `json:"itemname"`
	ItemCurrentPrice float64        `json:"itemcurrentprice"`
	ItemStrikedPrice float64        `json:"itemstrikedprice"`
	ItemBrand        string         `json:"itembrand"`
	ItemSoldOut      bool           `json:"itemsoldout"`
	IDUser           uint           `json:"-"`
}
