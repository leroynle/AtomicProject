package dto

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	UserID    uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
	FirstName string         `json:"firstname,omitempty"`
	LastName  string         `json:"lastname,omitempty"`
	IDAuth    uint           `json:"-"`
	Items     []Item         `gorm:"foreignkey:IDUser; references:UserID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE type:text[] " json:",omitempty" `
}
