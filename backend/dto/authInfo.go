package dto

import (
	"time"

	"gorm.io/gorm"
)

type Auth struct {
	AuthID    uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
	Email     string         `json:"email" binding:"required"`
	Password  string         `json:"password" binding:"required"`
	IsVerified bool 		`json:"isverified"`
}

type ChangePassword struct {
	Email       string `json:"email"`
	OldPassword string `json:"oldpassword"`
	NewPassword string `json:"newpassword"`
}

type ResetLink struct{
	Email string `json:"email" binding:"required"`
}

type ResetPassword struct {
	Password string `json:"password"`
	ConfirmPassword string `json:"confirmpassword"`
}
