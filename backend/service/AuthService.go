package service

import (
	"atomic/database"
	"atomic/dto"
	"atomic/util"
	"bytes"
	"fmt"
	"html/template"

	"github.com/alexedwards/argon2id"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	log "github.com/sirupsen/logrus"
)

func GetAuthByEmail(email string) (*dto.Auth, error) {
	db := database.GetDB()

	auth := dto.Auth{}
	if err := db.Where("email = ?", email).First(&auth).Error; err != nil {
		return nil, err
	}
	return &auth, nil
}

func HashPass(pass string) string {
	configure := util.HashPassConfig(".")
	params := &argon2id.Params{
		Memory:      configure.Memory * 1024, // the amount of memory used by the Argon2 algorithm (in kibibytes)
		Iterations:  configure.Iterations,    // the number of iterations (or passes) over the memory
		Parallelism: configure.Parallelism,   // the number of threads (or lanes) used by the algorithm
		SaltLength:  configure.SaltLength,    // length of the random salt. 16 bytes is recommended for password hashing
		KeyLength:   configure.KeyLength,     // length of the generated key (or password hash). 16 bytes or more is recommended
	}
	h, err := argon2id.CreateHash(pass, params)
	if err != nil {
		return "error"
	}
	return h
}

func SendEmail(subject string, body string, to string, html string, name string) bool {
	config, err := util.SendGridConfig(".")
	if err != nil {
		log.WithError(err)
	}
	from := mail.NewEmail("Support Team", config.Sendgrid_From_Email)
	_to := mail.NewEmail(name, to)
	plainTextContent := body
	htmlContent := html
	message := mail.NewSingleEmail(from, subject, _to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(config.Sendgrid_Api)
	res, err := client.Send(message)
	if err != nil {
		return false
	} else {
		fmt.Println(res.StatusCode)
		fmt.Println(res.Body)
		fmt.Println(res.Headers)
		return true
	}
}

func ParseHTMLTemplate(fileName string, data interface{}) (string, error) {
	t, err := template.ParseFiles(fileName)
	if err != nil {
		return "", err
	}
	buffer := new(bytes.Buffer)
	if err := t.Execute(buffer, data); err != nil {
		return "", err
	}
	return buffer.String(), nil
}
