package service

import (
	"atomic/dto"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

func GetItemDetails(item *dto.Item) {
	c := colly.NewCollector(
	// colly.AllowedDomains("nike.com", "www.nike.com"),
	//Allow visiting the same page multiple times
	// colly.AllowURLRevisit(),
	// // Allow crawling to be done in parallel / async
	// colly.Async(true),
	)

	c.OnHTML("#react-root", func(e *colly.HTMLElement) {

		item.ItemName = e.ChildText("#pdp_product_title")
		soldout := e.ChildText("div.sold-out > span.headline-5")
		if soldout == "" {
			item.ItemSoldOut = false
		} else {
			item.ItemSoldOut = true
		}

		price := e.ChildText("div.product-price.is--current-price")
		strikedPrice := e.ChildText("div.product-price.is--striked-out")
		price = price[1:]
		item.ItemCurrentPrice, _ = strconv.ParseFloat(price, 64)
		if strikedPrice != "" {
			strikedPrice = strikedPrice[1:]
			item.ItemStrikedPrice, _ = strconv.ParseFloat(strikedPrice, 64)
		}

		u, err := url.Parse(item.URL)
		if err != nil {
			log.Fatal(err)
		}
		parts := strings.Split(u.Hostname(), ".")
		item.ItemBrand = strings.Title(parts[len(parts)-2])
	})
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting: ", r.URL.String())
	})
	c.Visit(item.URL)
}
