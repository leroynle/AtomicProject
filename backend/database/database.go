package database

import (
	"atomic/util"
	"database/sql"
	"fmt"

	log "github.com/sirupsen/logrus"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gorm.io/driver/postgres"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var DB *gorm.DB
var sqlDB *sql.DB

func SetupDB() *gorm.DB {
	var db = DB
	config, err := util.DBConfig(".")
	if err != nil {
		log.WithError(err)
	}
	pg_conname := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=disable", config.Pg_host, config.Pg_port, config.Pg_user, config.Pg_db, config.Pg_password)
	sqlDB, err = sql.Open("postgres", pg_conname)
	if err != nil {
		log.WithError(err).Panic("panic code: 152")
	}
	sqlDB.SetMaxIdleConns(10)   // max number of connections in the idle connection pool
	sqlDB.SetMaxOpenConns(100)  // max number of open connections in the database
	sqlDB.SetConnMaxLifetime(1) // max amount of time a connection may be reused
	db, err = gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{
		Logger: logger.Default.LogMode(logger.LogLevel(1)),
	})
	if err != nil {
		log.WithError(err).Panic("panic code: 153")
	}
	// Only for debugging
	if err == nil {
		fmt.Println("DB connection successful!")
	}
	DB = db
	return DB
}
func GetDB() *gorm.DB {
	return DB
}
