package middleware

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

type myClaims struct {
	ID    uint   `json:"id"`
	Email string `json:"email"`
	jwt.StandardClaims
}

var MySigningKey []byte
var AuthID uint

func AuthorizeJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if len(authHeader) == 0 || !strings.Contains(authHeader, "Bearer ") {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		vals := strings.Split(authHeader, " ")
		if len(vals) != 2 {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		token, err := jwt.ParseWithClaims(vals[1], &myClaims{}, validateJWT)
		if err != nil {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if claims, ok := token.Claims.(*myClaims); ok && token.Valid {
			AuthID = claims.ID
		}
	}
}

func validateJWT(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("Invalid: %v", token.Header["alg"])
	}
	return MySigningKey, nil
}

func GenerateJWT(id uint, email string) (string, error) {
	claims := &myClaims{
		id,
		email,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
			Issuer:    "Leroy Le",
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//encoded string
	jwtValue, err := token.SignedString(MySigningKey)
	if err != nil {
		return "", err
	}
	return jwtValue, nil
}

func DecodeJWT(tokenStr string) (string, error){
	claims := myClaims{}
	var resAuth string
	token, err := jwt.ParseWithClaims(tokenStr, &claims, validateJWT)
	if err != nil{
		if err == jwt.ErrSignatureInvalid{
			return "", err
		}
		return "", err
	}
	if !token.Valid{
		return "", err
	}
	if claims, ok := token.Claims.(*myClaims); ok && token.Valid {
		resAuth = claims.Email
	}
	return resAuth, nil
}
