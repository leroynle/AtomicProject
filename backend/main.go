package main

import (

	// "github.com/gin-gonic/contrib/static"

	"atomic/controller"
	"atomic/database"
	"atomic/middleware"
	"fmt"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	if err := database.SetupDB().Error; err != nil {
		fmt.Println(err)
	}

	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOriginFunc:  func(origin string) bool { return true },
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	//serve frontend static files
	// router.Use(static.Serve("/", static.LocalFile("./views", true)))
	v1 := router.Group("/api/v1/")
	{
		rAuth := v1.Group("auths")
		rAuth.POST("register", controller.CreateUserAuth)
		rAuth.POST("login", controller.Login)
		rAuth.PUT("reset-link", controller.ResetPasswordLink)
		rAuth.POST("password-reset", controller.PasswordReset)
		rAuth.POST("verify-account", controller.VerifyAccount)
		rAuth.Use(middleware.AuthorizeJWT())
		rAuth.POST("password-change", controller.ChangePasswordAuth)

		//user group
		rUser := v1.Group("users")
		rUser.GET("/:id", controller.GetUserByID)
		rUser.Use(middleware.AuthorizeJWT())
		rUser.POST("", controller.CreateUser) // protected by JWT
		rUser.PUT("/:id", controller.UpdateUser)
		rUser.DELETE("/:id", controller.DeleteUser)

		rItem := v1.Group("items")
		rItem.GET("/:id", controller.GetItemByID)
		rItem.Use(middleware.AuthorizeJWT())
		rItem.POST("", controller.CreateItem)
		rItem.PUT("/:id", controller.UpdateItem)
		rItem.DELETE("/:id", controller.DeleteItem)
	}

	router.Run(":8080")
}
