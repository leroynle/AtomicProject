package util

import (
	"github.com/spf13/viper"
)

type dBconfig struct {
	Pg_user     string `mapstructure:"POSTGRES_USER"`
	Pg_password string `mapstructure:"POSTGRES_PASSWORD"`
	Pg_db       string `mapstructure:"POSTGRES_DB"`
	Pg_host     string `mapstructure:"POSTGRES_HOST"`
	Pg_port     string `mapstructure:"POSTGRES_PORT"`
}

func DBConfig(path string) (dbconfig dBconfig, err error) {
	viper.AddConfigPath(path)
	// viper.SetConfigName("")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&dbconfig)
	return
}
