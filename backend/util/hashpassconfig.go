package util

import (
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type hashPassParams struct {
	Memory      uint32
	Iterations  uint32
	Parallelism uint8
	SaltLength  uint32
	KeyLength   uint32
}
type hashPassMap struct {
	Hash_Memory      string `mapstructure:"HASHPASSMEMORY"`
	Hash_Iteration   string `mapstructure:"HASHPASSITERATIONS"`
	Hash_Parallelism string `mapstructure:"HASHPASSPARALLELISM"`
	Hash_SaltLength  string `mapstructure:"HASHPASSSALTLENGTH"`
	Hash_KeyLength   string `mapstructure:"HASHPASSKEYLENGTH"`
}

func HashPassConfig(path string) hashPassParams {
	// var config config
	var hpParams hashPassParams
	var configMap hashPassMap
	viper.AddConfigPath(path)
	// viper.SetConfigName("app")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		log.WithError(err)
	}

	err = viper.Unmarshal(&configMap)
	if err != nil {
		log.WithError(err)
	}

	hashPassMemory64, err := strconv.ParseUint(configMap.Hash_Memory, 10, 64)
	if err != nil {
		log.WithError(err).Panic("panic code: 121")
	}
	hashPassIterations64, err := strconv.ParseUint(configMap.Hash_Iteration, 10, 64)
	if err != nil {
		log.WithError(err).Panic("panic code: 122")
	}
	hashPassParallelism64, err := strconv.ParseUint(configMap.Hash_Parallelism, 10, 64)
	if err != nil {
		log.WithError(err).Panic("panic code: 123")
	}
	hashPassSaltLength64, err := strconv.ParseUint(configMap.Hash_SaltLength, 10, 64)
	if err != nil {
		log.WithError(err).Panic("panic code: 124")
	}
	hashPassKeyLength64, err := strconv.ParseUint(configMap.Hash_KeyLength, 10, 64)
	if err != nil {
		log.WithError(err).Panic("panic code: 125")
	}
	hpParams.Memory = uint32(hashPassMemory64)
	hpParams.Iterations = uint32(hashPassIterations64)
	hpParams.Parallelism = uint8(hashPassParallelism64)
	hpParams.SaltLength = uint32(hashPassSaltLength64)
	hpParams.KeyLength = uint32(hashPassKeyLength64)

	return hpParams
}
