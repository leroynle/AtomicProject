package util

import "github.com/spf13/viper"

type sendGridConfig struct{
	Sendgrid_Api string `mapstructure:"SENDGRID_API_KEY""`
	Sendgrid_From_Email string `mapstructure:"SENDGRID_FROM_EMAIL"`
}

func SendGridConfig(path string) (sgconfig sendGridConfig, err error) {
	viper.AddConfigPath(path)
	// viper.SetConfigName("")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&sgconfig)
	return
}