package controller

import (
	"atomic/database"
	"atomic/dto"
	"atomic/middleware"
	"atomic/service"
	"net"
	"net/http"
	"regexp"
	"strings"

	"github.com/alexedwards/argon2id"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func CreateUserAuth(ctx *gin.Context) {
	db := database.GetDB()
	auth := dto.Auth{}
	err := db.AutoMigrate(&auth)
	if err != nil {
		render(ctx, gin.H{"msg": "Internal server error"}, http.StatusInternalServerError)
		return
	}

	if err := ctx.ShouldBindJSON(&auth); err != nil {
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}

	if !isEmailValid(auth.Email) {
		render(ctx, gin.H{"msg": "invalid email address"}, http.StatusBadRequest)
		return
	}
	if err := db.Where("email = ?", auth.Email).First(&auth).Error; err == nil {
		render(ctx, gin.H{"msg": "email address registered"}, http.StatusBadRequest)
		return
	}

	tx := db.Begin()
	auth.Password = service.HashPass(auth.Password)
	auth.IsVerified = false
	if err := tx.Create(&auth).Error; err != nil {
		tx.Rollback()
		log.WithError(err).Error("error code: 700")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
	} else {
		tx.Commit()
		render(ctx, auth, http.StatusCreated)
	}

	verifyToken, _ := middleware.GenerateJWT(auth.AuthID, auth.Email)
	link := "http://" + ctx.Request.Host + "/api/v1/auths/verify-account?verify_token=" + verifyToken
	body, _ := service.ParseHTMLTemplate("templates/verify-account.html", map[string]string{"link_url": link})
	html := body
	isSentEmail := service.SendEmail("Verify Account", body, auth.Email, html, "")
	if isSentEmail == true {
		render(ctx, gin.H{"msg": "verification link has been sent"}, http.StatusOK)
	} else {
		render(ctx, gin.H{"msg": "there is something wrong with sending link"}, http.StatusInternalServerError)
	}

}

func Login(ctx *gin.Context) {
	loginCredential := dto.LoginCredential{}

	if err := ctx.ShouldBindJSON(&loginCredential); err != nil {
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}
	e, err := service.GetAuthByEmail(loginCredential.Email)
	if err != nil {
		render(ctx, gin.H{"msg": "email not found"}, http.StatusNotFound)
		return
	}

	match, err := argon2id.ComparePasswordAndHash(loginCredential.Password, e.Password)
	if err != nil {
		log.WithError(err).Error("error code :704")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		return
	}
	if !match {
		render(ctx, gin.H{"msg": "Wrong password!!"}, http.StatusUnauthorized)
		return
	}

	if e.IsVerified == false {
		render(ctx, gin.H{"msg": "The account is not verified"}, http.StatusNotFound)
		return
	}

	jwtValue, err := middleware.GenerateJWT(e.AuthID, e.Email)
	if err != nil {
		log.WithError(err).Error("error code: 705")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		return
	}

	render(ctx, gin.H{"jwt": jwtValue}, http.StatusOK)
}

func ChangePasswordAuth(ctx *gin.Context) {
	db := database.GetDB()
	auth := dto.Auth{}
	chpa := dto.ChangePassword{}

	auth.AuthID = middleware.AuthID

	if err := ctx.ShouldBindJSON(&chpa); err != nil {
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}

	e, err := service.GetAuthByEmail(chpa.Email)
	if err != nil {
		render(ctx, gin.H{"msg": "email not found"}, http.StatusNotFound)
		return
	}

	if e.AuthID != auth.AuthID {
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}

	match, err := argon2id.ComparePasswordAndHash(chpa.OldPassword, e.Password)
	if err != nil {
		log.WithError(err).Error("error code :701")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		return
	}
	if !match {
		render(ctx, gin.H{"msg": "Wrong password!!"}, http.StatusUnauthorized)
		return
	}
	tx := db.Begin()
	auth.Email = chpa.Email
	auth.Password = service.HashPass(chpa.NewPassword)
	if err := db.Save(&auth).Error; err != nil {
		tx.Rollback()
		log.WithError(err).Error("error code: 702")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
	} else {
		tx.Commit()
		render(ctx, gin.H{"msg": "password updated successfully"}, http.StatusOK)
	}

}

func ResetPasswordLink(ctx *gin.Context) {
	data := dto.ResetLink{}

	if err := ctx.ShouldBindJSON(&data); err != nil {
		render(ctx, gin.H{"msg": "not acceptable"}, http.StatusNotAcceptable)
		return
	}
	e, err := service.GetAuthByEmail(data.Email)
	if err != nil {
		render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
		return
	}

	resetToken, _ := middleware.GenerateJWT(e.AuthID, e.Email)
	link := ctx.Request.Host + "/api/v1/auths/password-reset?reset_token=" + resetToken
	body, _ := service.ParseHTMLTemplate("templates/reset-password.html", map[string]string{"link_url": link})
	html := body
	isSentEmail := service.SendEmail("Reset Password", body, e.Email, html, "Leroy")
	if isSentEmail == true {
		render(ctx, gin.H{"msg": "Sent Email"}, http.StatusOK)
		return
	} else {
		render(ctx, gin.H{"msg": "An error occurred when sending email"}, http.StatusInternalServerError)
		return
	}
}

func PasswordReset(ctx *gin.Context) {
	db := database.GetDB()
	data := dto.ResetPassword{}
	auth := dto.Auth{}
	if err := ctx.ShouldBindJSON(&data); err != nil {
		render(ctx, gin.H{"msg": "not acceptable"}, http.StatusNotAcceptable)
		return
	}
	if data.Password != data.ConfirmPassword {
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}

	resetToken, _ := ctx.GetQuery("reset_token")
	authEmail, _ := middleware.DecodeJWT(resetToken)
	e, err := service.GetAuthByEmail(authEmail)
	if err != nil {
		render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
		return
	}
	tx := db.Begin()
	auth.AuthID = e.AuthID
	auth.Email = e.Email
	auth.Password = service.HashPass(data.Password)
	if err := db.Save(&auth).Error; err != nil {
		tx.Rollback()
		log.WithError(err).Error("error code: 703")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
	} else {
		tx.Commit()
		render(ctx, gin.H{"msg": "password successfully updated"}, http.StatusOK)
	}
}

func verifyAccountLink(ctx *gin.Context) {
	data := dto.ResetLink{}
	if err := ctx.ShouldBindJSON(&data); err != nil {
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}
	e, err := service.GetAuthByEmail(data.Email)
	if err != nil {
		render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
		return
	}
	verifyToken, _ := middleware.GenerateJWT(e.AuthID, e.Email)
	link := ctx.Request.Host + "/api/v1/auths/verify-account?verify_token=" + verifyToken
	body, _ := service.ParseHTMLTemplate("templates/verify-account.html", map[string]string{"link_url": link})
	html := body

	isSentEmail := service.SendEmail("Verify Account", body, e.Email, html, "")
	if isSentEmail == true {
		render(ctx, gin.H{"msg": "verification link has been sent"}, http.StatusOK)
		return
	} else {
		render(ctx, gin.H{"msg": "there is something wrong with sending link"}, http.StatusInternalServerError)
		return
	}
}

func VerifyAccount(ctx *gin.Context) {
	db := database.GetDB()
	auth := dto.Auth{}
	verifyToken, _ := ctx.GetQuery("verify_token")
	authEmail, _ := middleware.DecodeJWT(verifyToken)
	e, err := service.GetAuthByEmail(authEmail)
	if err != nil {
		render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
		return
	}
	tx := db.Begin()
	auth.AuthID = e.AuthID
	auth.Email = e.Email
	auth.Password = e.Password
	auth.IsVerified = true
	if err := db.Save(&auth).Error; err != nil {
		tx.Rollback()
		log.WithError(err).Error("error code:704")
		render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
	} else {
		tx.Commit()
		render(ctx, gin.H{"msg": "Account has been verified"}, http.StatusOK)
	}

}

// isEmailValid checks if the email provided passes the required structure
// and length test. It also checks the domain has a valid MX record.
// Credit: Edd Turtle
func isEmailValid(e string) bool {
	var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if len(e) < 3 || len(e) > 254 {
		return false
	}

	if !emailRegex.MatchString(e) {
		return false
	}

	parts := strings.Split(e, "@")
	mx, err := net.LookupMX(parts[1])
	if err != nil || len(mx) == 0 {
		return false
	}

	return true
}
