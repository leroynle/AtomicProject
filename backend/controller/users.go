package controller

import (
	"atomic/database"
	"atomic/dto"
	"atomic/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func GetUserByID(ctx *gin.Context) {
	db := database.GetDB()
	id := ctx.Param("id")
	user := dto.User{}
	items := []dto.Item{}
	if err := db.Where("user_id = ?", id).First(&user).Error; err != nil {
		render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
	} else {
		db.Model(&items).Where("id_user= ?", id).Find(&items)
		user.Items = items
		render(ctx, user, http.StatusOK)
	}
}

func CreateUser(ctx *gin.Context) {
	db := database.GetDB()
	users := dto.User{}
	err := db.AutoMigrate(&users)
	if err != nil {
		return 
	}

	createdUser := 0
	users.IDAuth = middleware.AuthID

	if err := ctx.ShouldBindJSON(&users); err != nil {
		log.WithError(err)
	}
	if err := db.Where("id_auth = ?", users.IDAuth).First(&users).Error; err == nil {
		createdUser = 1
		render(ctx, gin.H{"msg": "user has been created"}, http.StatusBadRequest)
		return
	}

	if createdUser == 0 {
		ctx.ShouldBindJSON(&users)
		tx := db.Begin()
		if err := tx.Create(&users).Error; err != nil {
			tx.Rollback()
			log.WithError(err).Error("error code: 800")
			render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		} else {
			tx.Commit()
			render(ctx, users, http.StatusCreated)
		}
	}
}

func UpdateUser(ctx *gin.Context) {
	db := database.GetDB()
	users := dto.User{}
	id := ctx.Param("id")
	updatedUser := 0
	users.IDAuth = middleware.AuthID
	if err := db.Where("id_auth = ?", users.IDAuth).First(&users).Error; err != nil {
		updatedUser = 1
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}
	if updatedUser == 0 {
		if err := db.Where("user_id = ?", id).First(&users).Error; err != nil {
			updatedUser = 1
			render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
			return
		}
	}
	if updatedUser == 0 {
		ctx.ShouldBindJSON(&users)
		tx := db.Begin()
		if err := tx.Save(&users).Error; err != nil {
			tx.Rollback()
			log.WithError(err).Error("error code: 801")
			render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		} else {
			tx.Commit()
			render(ctx, &users, http.StatusOK)
		}
	}
}
func DeleteUser(ctx *gin.Context) {
	db := database.GetDB()
	users := dto.User{}
	id := ctx.Param("id")
	deletedUser := 0
	users.IDAuth = middleware.AuthID
	if err := db.Where("id_auth = ?", users.IDAuth).First(&users).Error; err != nil {
		deletedUser = 1
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadGateway)
		return
	}
	if deletedUser == 0 {
		if err := db.Where("user_id = ?", id).First(&users).Error; err != nil {
			deletedUser = 1
			render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
			return
		}
	}
	if deletedUser == 0 {
		ctx.ShouldBindJSON(&users)

		tx := db.Begin()
		if err := tx.Delete(&users).Error; err != nil {
			tx.Rollback()
			log.WithError(err).Error("error code: 802")
			render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		} else {
			tx.Commit()
			render(ctx, gin.H{"msg": "Deleted the user"}, http.StatusOK)
		}
	}
}
