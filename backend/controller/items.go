package controller

import (
	"atomic/database"
	"atomic/dto"
	"atomic/middleware"
	"atomic/service"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func CreateItem(ctx *gin.Context) {
	db := database.GetDB()
	items := dto.Item{}
	users := dto.User{}
	err := db.AutoMigrate(&items)
	if err != nil {
		return
	}
	createdItem := 0

	users.IDAuth = middleware.AuthID
	if err := ctx.ShouldBindJSON(&items); err != nil {
		log.WithError(err)
	}

	if err := db.Where("id_auth = ?", users.IDAuth).First(&users).Error; err != nil {
		createdItem = 1 // user not registered
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}
	if createdItem == 0 {
		err := ctx.ShouldBindJSON(&items)
		if err != nil {
			return
		}
		items.IDUser = users.UserID
		service.GetItemDetails(&items)
		tx := db.Begin()
		if err := tx.Create(&items).Error; err != nil {
			tx.Rollback()
			log.WithError(err).Error("error code : 900")
			render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		} else {
			tx.Commit()
			render(ctx, items, http.StatusCreated)
		}
	}
}

func GetItemByID(ctx *gin.Context) {
	db := database.GetDB()
	id := ctx.Param("id")
	item := dto.Item{}
	if err := db.Where("item_id= ?", id).First(&item).Error; err != nil {
		render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
	} else {
		render(ctx, item, http.StatusOK)
	}
}

func DeleteItem(ctx *gin.Context) {
	db := database.GetDB()
	id := ctx.Param("id")
	user := dto.User{}
	item := dto.Item{}
	deletedItem := 0
	user.IDAuth = middleware.AuthID
	if err := db.Where("id_auth = ?", user.IDAuth).First(&user).Error; err != nil {
		deletedItem = 1 // not registered
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}
	if deletedItem == 0 {
		if err := db.Where("item_id = ?", id).First(&item).Error; err != nil {
			render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
			return
		}
	}
	if deletedItem == 0 {
		err := ctx.ShouldBindJSON(&item)
		if err != nil {
			render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
			return
		}
		tx := db.Begin()
		if err := db.Delete(&item).Error; err != nil {
			tx.Rollback()
			log.WithError(err).Error("error code: 902")
			render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		} else {
			tx.Commit()
			render(ctx, gin.H{"msg": "Deleted the item"}, http.StatusOK)
		}
	}
}

func UpdateItem(ctx *gin.Context) {
	db := database.GetDB()
	items := dto.Item{}
	users := dto.User{}
	id := ctx.Param("id")

	updatedItem := 0

	users.IDAuth = middleware.AuthID
	if err := db.Where("id_auth = ?", users.IDAuth).First(&users).Error; err != nil {
		updatedItem = 1
		render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
		return
	}
	if updatedItem == 0 {
		if err := db.Where("item_id = ?", id).First(&items).Error; err != nil {
			updatedItem = 1
			render(ctx, gin.H{"msg": "not found"}, http.StatusNotFound)
			return
		}
	}
	if updatedItem == 0 {
		err := ctx.ShouldBindJSON(&items)
		if err != nil {
			render(ctx, gin.H{"msg": "bad request"}, http.StatusBadRequest)
			return
		}
		tx := db.Begin()
		if err := tx.Save(&items).Error; err != nil {
			tx.Rollback()
			log.WithError(err).Error("error code : 901")
			render(ctx, gin.H{"msg": "internal server error"}, http.StatusInternalServerError)
		} else {
			tx.Commit()
			render(ctx, items, http.StatusOK)
		}
	}
}
